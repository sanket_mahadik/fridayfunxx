/*
 * Copyright (c) 2014 by Gerrit Grunwald
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.hansolo.fx;

import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


/**
 * User: hansolo
 * Date: 28.11.14
 * Time: 12:20
 */
public class Demo extends Application {
    private AvGauge control;

    @Override public void init() {
        control = AvGaugeBuilder.create()
                                .title("Title")
                                .multiColor(false)
                                .animationDuration(10000)
                                .decimals(0)
                                .build();

        control.outerCurrentValueProperty().addListener((ov, o, n) -> {

        });
    }

    @Override public void start(Stage stage) {
        StackPane pane = new StackPane();
        pane.getChildren().addAll(control);

        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.show();

        control.setOuterValue(90);
        control.setInnerValue(35);
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
